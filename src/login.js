
document.addEventListener("DOMContentLoaded", function(){
	var plugin;
        var dev;
        let cont = document.getElementById("cont");
        let public_key = "";
        let data_hash = "";

        
		rutoken.ready.then(function() {
            if (window.chrome) {
                return rutoken.isExtensionInstalled();
            } else {
                return Promise.resolve(true);
            }
		}).then(function(result){

			if (result) {
                return rutoken.isPluginInstalled();
            } else {
            	throw "Браузер временно не поддерживается. Пожалуйста, воспользуйтесь браузером Apple Safari или Mozilla Firefox.";
            }
		}).then(function(result) {
            if (result) {
                return rutoken.loadPlugin();
            } else {
            	throw "not loaded";
            }
        }).then(function(p) {
        	//console.log(plugin);
        
        	plugin = p;
            console.log("plugin loaded");
        	return plugin.enumerateDevices();
        	

        }).then( function(devs){
            if(devs.length==0){
                throw "Устройство не найдено"
            }
            dev = devs[0];
            

            plugin.login(devs[0],pin)
            console.log("device:"+ dev );
        	
            


            /*
        	return Promise.all([
               plugin.deleteKeyPair(dev, "bd:15:15:b7:b2:39:a7:ac:19:3e:b2:16:d5:d2:09:e7:fb:ed:a7:fc"),
               plugin.deleteKeyPair(dev, "bd:15:15:b7:b2:39:a7:ac:19:3e:b2:16:d5:d2:09:e7:fb:ed:a7:fb"),
               plugin.deleteKeyPair(dev, "bd:15:15:b7:b2:39:a7:ac:19:3e:b2:16:d5:d2:09:e7:fb:ed:a7:fa")  
               ]);
            */
            return true;
            
        }).then( function(){
            return plugin.enumerateKeys(dev, "");

        }).then( function(keys){
        	console.log(keys);
            return plugin.generateKeyPair(dev, undefined, "token1", 
                {id: keyid,
                publicKeyAlgorithm:plugin.PUBLIC_KEY_ALGORITHM_GOST3410_2001,
                paramset:"A",

            }) 
        }).then( function(key){
            console.log("key:"+key);
            keyid = key;
            return plugin.digest(dev, plugin.HASH_TYPE_GOST3411_94 , data, {base64:false})

            
        }).then( function(hash){
            
            console.log("hash:"+hash);
            data_hash = hash;
            return plugin.getPublicKeyValue(0, keyid, {})
            
            
        }).then( function(pkey){
            public_key = pkey;
            
            console.log("sing with pkey:"+pkey+" data:"+data+ " hash:" + data_hash)

            return plugin.rawSign(dev, keyid, data_hash , {computeHash:false}) 
        }).then( function(sign){
            console.log("sing:"+sign);
        }).catch( function(error){
        	console.log( error );
        })
		
        

});