import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import * as actions from './store/actions'
Vue.use(Vuex)
Vue.config.productionTip = false

const store = new Vuex.Store({
  strict: true,
  state: {
    loading: false,
    lid: null
  },
  actions,
  mutations: {
    setLoading (state, isloading) {
      state.loading = isloading
    },

    setLoginStatus(state, lid){
      state.lid = lid 
    },

    setLoading(state, loading){
      state.loading = loading
    }

  }
})

/* eslint-disable no-new */
var vm=new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App) 
})


