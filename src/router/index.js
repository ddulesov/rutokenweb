import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Info from '@/components/Info'
import Login from '@/components/Login'
import Error404 from '@/components/Error404'

import { SET_NAME } from '@/store/mutations'
Vue.use(Router)

const router = new Router({
  /* mode: 'history', */
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      meta: {  title:'главная' }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {  title:'авторизация' }
    },
    {
      path: '/info',
      name: 'Info',
      component: Info,
      meta: {  title:'содержание',requiresAuth:true }
    },
    {
      path: '*',
      component: Error404,
      meta: { requiresAuth: false }
    }
  ]
})

router.beforeEach((to, from, next) => {
  var token = window.localStorage.getItem('token');	

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (token==null) {
      //console.log("redirect to login");
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // всегда так или иначе нужно вызвать next()!
  }
})
export default router
