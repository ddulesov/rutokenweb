package main

import (
	// "cypherpunks.ru/gogost/gost3410"
	"encoding/hex"
	"log"
	"strings"
	"testing"
)

const keyid string = "bd:15:15:b7:b2:39:a7:ac:19:3e:b2:16:d5:d2:09:e7:fb:ed:a7:fd" //info only. RuToken keyid
const hashval string = "ae:23:0a:b8:39:5b:df:cf:76:61:ab:3f:23:42:04:4a:e7:6c:55:cf:2e:7e:ff:32:73:5e:e3:da:c0:f3:d8:70"
const public_key string = "07:b0:c8:ab:6b:4f:44:6c:03:07:79:89:63:15:88:f8:83:ba:92:e2:76:a0:f2:90:85:e0:c1:85:b8:16:73:8f:b5:b5:9c:70:dd:0c:63:a6:7f:46:45:2f:13:b8:b4:f3:15:40:33:87:94:2f:18:61:0d:44:59:ac:cb:da:93:f9"
const data string = "0123456789"

const sign string = "45:33:f7:10:44:ab:35:ac:af:97:20:e8:93:79:60:d0:20:b3:72:63:4c:21:90:aa:30:a9:14:1f:8d:5b:9f:d6:fa:d6:3a:fb:0a:a0:63:4c:83:c9:fc:d7:5e:02:36:7f:9a:18:eb:34:79:3e:6b:f5:40:99:b0:d9:01:9f:63:07"

func unhex(str string) []byte {

	decoded, err := hex.DecodeString(strings.Replace(str, ":", "", -1))
	if err != nil {
		log.Fatal(err)
	}
	return decoded
}

func TestRuTokenWeb(t *testing.T) {
	digest := unhex(hashval)
	reverse(digest)

	signeter := unhex(sign)

	log.Printf("digest: %s", hex.Dump(digest))
	log.Printf("signature: %s", hex.Dump(signeter))

	ec, err := NewCurveFromParams(CurveParamsGostR34102001CryptoProA)
	if err != nil {
		t.Fatal(err)
	}

	pubBytes := unhex(public_key)
	pubKey, err := NewPublicKey(ec, Mode2001, pubBytes)

	if err != nil {
		t.Fatal(err)
	}
	if !pubKey.Verify() {
		t.Error("wrong public key ")
		t.Fatal("public key x,y are not correct")
	}

	log.Printf("pub key: %s", hex.Dump(pubBytes))
	valid, err := pubKey.VerifyDigest(digest[:], signeter)

	if err != nil {
		t.Fatal(err)
	}

	if !valid {
		t.Error("signature failed")
	}

}

func BenchmarkVerify(b *testing.B) {
	digest := unhex(hashval)
	reverse(digest)

	signeter := unhex(sign)
	ec, err := NewCurveFromParams(CurveParamsGostR34102001CryptoProA)
	if err != nil {
		b.Fatal(err)
	}

	pubBytes := unhex(public_key)
	pubKey, err := NewPublicKey(ec, Mode2001, pubBytes)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		valid, err := pubKey.VerifyDigest(digest[:], signeter)
		if err != nil || !valid {
			b.Log("not valid")
		}
	}
}

/*
func BenchmarkVerify(b *testing.B) {
	digest := unhex(hashval)
	reverse(digest)

	signeter := unhex(sign)
	ec, err := gost3410.NewCurveFromParams(gost3410.CurveParamsGostR34102001CryptoProA)
	if err != nil {
		b.Fatal(err)
	}

	pubBytes := unhex(public_key)
	pubKey, err := gost3410.NewPublicKey(ec, gost3410.Mode2001, pubBytes)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		valid, err := pubKey.VerifyDigest(digest[:], signeter)
		if err != nil || !valid {
			b.Log("not valid")
		}
	}
}
*/
