package main

import (
	"math/big"
)

var (
	zero    *big.Int = big.NewInt(0)
	bigInt1 *big.Int = big.NewInt(1)
	bigInt2 *big.Int = big.NewInt(2)
	bigInt3 *big.Int = big.NewInt(3)
)

type Curve struct {
	P *big.Int
	Q *big.Int
	A *big.Int
	B *big.Int

	// Basic point X and Y coordinates
	Gx *big.Int
	Gy *big.Int

	// Temporary variable for the add method
	/*
		t  *big.Int
		tx *big.Int
		ty *big.Int
	*/
}

func NewCurve(p, q, a, b, bx, by []byte) (*Curve, error) {
	c := Curve{
		P:  bytes2big(p[:]),
		Q:  bytes2big(q[:]),
		A:  bytes2big(a[:]),
		B:  bytes2big(b[:]),
		Gx: bytes2big(bx[:]),
		Gy: bytes2big(by[:]),
	}
	/*
		r1 := big.NewInt(0)
		r2 := big.NewInt(0)
		r1.Mul(c.By, c.By)
		r1.Mod(r1, c.P)
		r2.Mul(c.Bx, c.Bx)
		r2.Add(r2, c.A)
		r2.Mul(r2, c.Bx)
		r2.Add(r2, c.B)
		r2.Mod(r2, c.P)
		if r2.Cmp(big.NewInt(0)) == -1 {
			r2.Add(r2, c.P)
		}
		if r1.Cmp(r2) != 0 {
			return nil, errors.New("Invalid curve parameters")
		}
		return &c, nil
	*/
	if c.InCurve(c.Gx, c.Gy) {
		return &c, nil
	}
	return nil, errInvalidParameter
}

func (c *Curve) InCurve(x *big.Int, y *big.Int) bool {
	r1 := new(big.Int)
	r2 := new(big.Int)
	r1.Mul(y, y)
	r1.Mod(r1, c.P)
	r2.Mul(x, x)
	r2.Add(r2, c.A)
	r2.Mul(r2, x)
	r2.Add(r2, c.B)
	r2.Mod(r2, c.P)

	/*
		if r2.Cmp(zero) == -1 {
			r2.Add(r2, c.P)
		}
	*/
	c.pos(r2)

	return r1.Cmp(r2) == 0

}

func (c *Curve) pos(v *big.Int) {
	if v.Cmp(zero) < 0 {
		v.Add(v, c.P)
	}
}

func (c *Curve) add(p1x, p1y, p2x, p2y *big.Int) {
	t := new(big.Int)
	tx := new(big.Int)
	ty := new(big.Int)

	if p1x.Cmp(p2x) == 0 && p1y.Cmp(p2y) == 0 {
		// double
		t.Mul(p1x, p1x)
		t.Mul(t, bigInt3)
		t.Add(t, c.A)
		tx.Mul(bigInt2, p1y)
		tx.ModInverse(tx, c.P)
		t.Mul(t, tx)
		t.Mod(t, c.P)
	} else {
		tx.Sub(p2x, p1x)
		tx.Mod(tx, c.P)
		c.pos(tx)
		ty.Sub(p2y, p1y)
		ty.Mod(ty, c.P)
		c.pos(ty)
		t.ModInverse(tx, c.P)
		t.Mul(t, ty)
		t.Mod(t, c.P)
	}
	tx.Mul(t, t)
	tx.Sub(tx, p1x)
	tx.Sub(tx, p2x)
	tx.Mod(tx, c.P)
	c.pos(tx)
	ty.Sub(p1x, tx)
	ty.Mul(ty, t)
	ty.Sub(ty, p1y)
	ty.Mod(ty, c.P)
	c.pos(ty)
	p1x.Set(tx)
	p1y.Set(ty)
}

func (c *Curve) Exp(degree, xS, yS *big.Int) (*big.Int, *big.Int, error) {
	if degree.Cmp(zero) == 0 {
		return nil, nil, errInvalidParameter
	}

	dg := new(big.Int).Sub(degree, bigInt1)
	tx := new(big.Int).Set(xS)
	ty := new(big.Int).Set(yS)
	cx := new(big.Int).Set(xS)
	cy := new(big.Int).Set(yS)

	for dg.Cmp(zero) != 0 {
		if dg.Bit(0) == 1 {
			c.add(tx, ty, cx, cy)
		}
		dg.Rsh(dg, 1)
		c.add(cx, cy, cx, cy)
	}
	return tx, ty, nil
}
