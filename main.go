package main

import (
	//"context"
	//"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"
)

var (
	rnd     *rand.Rand
	ec      *Curve
	rootDir string
)

type JError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func newHash() []byte {
	var p [32]byte
	_, err := rnd.Read(p[:])
	if err != nil {
		log.Fatal(err)
	}

	return p[:]
}

type JHash struct {
	Key string `json:"key,omitempty"`
}

func NewHash() *JHash {
	var h JHash
	h.Key = hex.EncodeToString(newHash())
	return &h
}

type JSignedHash struct {
	JHash
	Sign string `json:"sign"`
	Id   string `json:"id"`
}

func toJSON(w http.ResponseWriter, val interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(val)
}

func fromJSON(r *http.Request, u interface{}) error {
	err := json.NewDecoder(r.Body).Decode(u)
	if err != nil {
		return err
	}
	return nil
}

func toErrorCode(w http.ResponseWriter, status int, code int, message string) {
	e := JError{Code: code, Message: message}
	w.WriteHeader(status)
	toJSON(w, e)
}

func Abort(w http.ResponseWriter) {
	toErrorCode(w, 400, 0, "")
}
func AbortWithError(w http.ResponseWriter, err error) {
	toErrorCode(w, 400, 0, err.Error())
}

func mainHandler(w http.ResponseWriter, r *http.Request) {

	//http.Redirect(w, r, url, http.StatusFound)
}

func setHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		fmt.Fprintf(w, "setHandler")
		return
	}

	var res JSignedHash
	err := fromJSON(r, &res)

	if err != nil {
		AbortWithError(w, err)
		return
	}

	keyFile := path.Join(rootDir, "keys/"+res.Id+".key")

	pubKeyBytes, err := hex.DecodeString(res.Sign)
	pubKey, err := NewPublicKey(ec, Mode2001, pubKeyBytes)

	if !pubKey.Verify() {
		AbortWithError(w, err)
		return
	}

	err = ioutil.WriteFile(keyFile, pubKeyBytes, 0644)
	if err != nil {
		AbortWithError(w, err)
		return
	}

}

func authHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//h := sha256.New()
		//h.Write([]byte("hello world\n"))
		//fmt.Printf("%x", h.Sum(nil))

		res := NewHash()
		log.Printf("new hash: %s", res.Key)
		toJSON(w, res)
		return
	}

	var res JSignedHash
	err := fromJSON(r, &res)

	if err != nil {
		AbortWithError(w, err)
		return
	}

	if len(res.Id) != 40 || len(res.Sign) != 128 {
		toErrorCode(w, 400, 1, "incorrect data provided")
		return
	}

	keyFile := path.Join(rootDir, "keys/"+res.Id+".key")

	if _, err := os.Stat(keyFile); err != nil {
		toErrorCode(w, 400, 1, "key "+res.Id+" not registered")
		return
	}
	pubKeyBytes, err := ioutil.ReadFile(keyFile)
	if err != nil {
		AbortWithError(w, err)
		return
	}

	pubKey, err := NewPublicKey(ec, Mode2001, pubKeyBytes)
	if err != nil {
		AbortWithError(w, err)
		return
	}

	signature, err := hex.DecodeString(res.Sign)
	if err != nil {
		toErrorCode(w, 400, 1, "incorrect data provided")
		return
	}

	digest, err := hex.DecodeString(res.Key)
	if err != nil || len(digest) != 32 {
		toErrorCode(w, 400, 1, "incorrect key provided")
		return
	}
	reverse(digest)
	if ok, _ := pubKey.VerifyDigest(digest, signature); !ok {

		toErrorCode(w, 400, 2, "key not valid")
	}

}

func main() {
	var err error
	fmt.Printf("test GOST &  RuToken Web ")

	rootDir, err = filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	rnd = rand.New(rand.NewSource(time.Now().UnixNano()))
	ec, err = NewCurveFromParams(CurveParamsGostR34102001CryptoProA)

	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", mainHandler)
	http.HandleFunc("/api/register/", setHandler)
	http.HandleFunc("/api/auth/", authHandler)

	http.ListenAndServe(":8081", nil)
}
